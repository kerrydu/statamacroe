sjlog using logtreg1, replace
use ps2009
egen id=group(country)
xtset id year
generate lnpgdp=ln(pgdp)
pfilter lnpgdp, method(hp) trend(lnpgdp2) smooth(400)
sjlog close, replace

sjlog using logtreg2, replace
logtreg lnpgdp2,  kq(0.333)
sjlog close, replace


sjlog using logtreg3, replace
psecta lnpgdp2, name(country) kq(0.333) gen(club) noprt
matrix b=e(bm)
matrix t=e(tm)
matrix result1=(b \ t)
matlist result1, border(rows) rowtitle("log(t)") format(%9.3f) left(4)
sjlog close, replace


sjlog using logtreg4, replace
scheckmerge lnpgdp2,  kq(0.333) club(club) mdiv
matrix b=e(bm)
matrix t=e(tm)
matrix result2=(b \ t)
matlist result2, border(rows) rowtitle("log(t)") format(%9.3f) left(4)
imergeclub lnpgdp2, name(country) kq(0.333) club(club) gen(finalclub) noprt
matrix b=e(bm)
matrix t=e(tm)
matrix result3=(b \ t)
matlist result3, border(rows) rowtitle("log(t)") format(%9.3f) left(4)
sjlog close, replace


